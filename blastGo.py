from sys import exit
from os import listdir, system, remove
from os.path import isfile, join, exists

class blastGo:
	#################################################################
	#####                                                       #####
	#####                   CLASS PROPERTIES                    #####
	#####                                                       #####
	#################################################################
	# @subjectFullPath - full or relative path to subject or reference folder
	# @subjectFilenameList - list of all filenames in @subjectFullPath
	# @queryFullPath - full or relative path to query or input folder
	# @queryFilenameList - list of all filenames in @queryFullPath
	# @outputFullPath - path on system where output files will be written
	# @outputFilenameList - list of all filenames in @outputFullPath
	# @programPath - full(preferable) or relative path to blat binary file
	# @programParameterString - full string of blat optional parameter
	subjectFullPath = ''
	subjectFilenameList = []
	queryFullPath = ''
	queryFilenameList = []
	outputFullPath = ''
	outputFilenameList = []

	# @programPath[string]: full path to the folder containing BLAST binary file
	# @programNames[list]: list of all available program names
	# @programToUse[string]: indicate which BLAST to be use from the
	#						 available options in @programNames
	# @programParameterString[string]: optional parameter string

	programPath = ''
	programNames = ['blastn', 'blastp']
	programToUse = ''
	programParameterString = ''

	# @outputFormats[dictionary]: contain output format option with its number
	# @outputFormatToUse[string]: output format write the file out
	# @outputHeaders[dictionary]: all possible output headers to be specify when
	#							  output format is specify to be 'tabular' or 'csv'
	# @outputHeaderColumn[list]: list of informations to put in output
	#							 if the output format is 'tabular' or 'csv'
	# @outputSeperator[dictionary]: contain column seperator character
	#								for each parsable output format

	outputFormats = {'pairwise': 0, 'xml': 5, 'tabular': 6, 'commented_tabular': 7, 'csv': 10}
	outputFormatToUse = ''
	outputHeaders = { 'qseqid': 'Query Seq-id', 
					  'qgi': 'Query GI',
					  'qacc': 'Query accesion',
					  'qaccver': 'Query accesion.version',
					  'qlen': 'Query sequence length',
					  'sseqid': 'Subject Seq-id',
					  'sallseqid': 'All subject Seq-id(s), separated by a ";"',
					  'sgi': 'Subject GI',
					  'sallgi': 'All subject GIs',
					  'sacc': 'Subject accession',
					  'saccver': 'Subject accession.version',
					  'sallacc': 'All subject accessions',
					  'slen': 'Subject sequence length',
					  'qstart': 'Start of alignment in query',
					  'qend': 'End of alignment in query',
					  'sstart': 'Start of alignment in subject',
					  'send': 'End of alignment in subject',
					  'qseq': 'Aligned part of query sequence',
					  'sseq': 'Aligned part of subject sequence',
					  'evalue': 'Expect value',
					  'bitscore': 'Bit score',
					  'score': 'Raw score',
					  'length': 'Alignment length',
					  'pident': 'Percentage of identical matches',
					  'nident': 'Number of identical matches',
					  'mismatch': 'Number of mismatches',
					  'positive': 'Number of positive-scoring matches',
					  'gapopen': 'Number of gap openings',
					  'gaps': 'Total number of gaps',
					  'ppos': 'Percentage of positive-scoring matches',
					  'frames': 'Query and subject frames separated by a "/"',
					  'qframe': 'Query frame',
					  'sframe': 'Subject frame',
					  'btop': 'Blast traceback operations (BTOP)' }
	outputHeaderColumn = ['qseqid', 'sseqid', 'pident', 'length', 'mismatch', 'gapopen', 'qstart', 'qend', 'sstart', 'send', 'evalue', 'bitscore']
	outputSeperator = {'tabular': '\t', 'commented_tabular': '\t', 'csv': ','}
	#################################################################


	#################################################################
	#####                                                       #####
	#####                   CLASS CONSTRUCTOR                   #####
	#####                                                       #####
	#################################################################
	# Initiate class with required parameters and load all filenames
	# ========================== INPUT ==============================
	# @subjectFullPath[string]: full path to the folder containing subject
	#							or reference files
	# @queryFullPath[string]: full path to the folder containing input query files
	# @outputFullPath[string]: full path to the folder to contain output files
	# @programPath[string]: full path to the folder containing PRODIGAL binary file
	# @programParameterString[string]: optional parameter string
	#################################################################
	def __init__(self, subjectFullPath, queryFullPath, outputFullPath):
		# assigning class data
		self.subjectFullPath = subjectFullPath
		self.queryFullPath = queryFullPath
		self.outputFullPath = outputFullPath
		# check if paths ends with '/'
		# if not, add '/' to path data
		if not subjectFullPath[-1] == '/':
			self.subjectFullPath += '/'
		if not queryFullPath[-1] == '/':
			self.queryFullPath += '/'
		if not outputFullPath[-1] == '/':
			self.outputFullPath += '/'
		# load list of subject and query filenames into class
		self.loadFilenameList('subject')
		self.loadFilenameList('query')
		self.outputFilenameList = []


	######################### CLASS METHOD ##########################
	#####                   loadFilenameList                    #####
	#################################################################
	# Load the indicated filename list into class properties
	# ========================== INPUT ==============================
	# @list_indicator[string]: indicate which list of file to be load
	#								'subject', 'query', or 'output'
	#################################################################
	def loadFilenameList(self, list_indicator = False):
		# ==================== LOCAL VARIABLES ======================
		# @f[file]: temporary variable to keep each filename while checking
		# ===========================================================

		# printing progress to terminal
		if list_indicator == False or list_indicator == 'subject':
			print 'Loading file list from Subject input folder.'
			# list everything from @subjectFullPath and append to @ subjectFilenameList if it is a file
			self.subjectFilenameList = [ f for f in listdir(self.subjectFullPath) if isfile(join(self.subjectFullPath, f)) ]

		if list_indicator == False or list_indicator == 'query':
			print 'Loading file list from Query input folder.'
			# list everything from @queryFullPath and append to @ queryFilenameList if it is a file
			self.queryFilenameList = [ f for f in listdir(self.queryFullPath) if isfile(join(self.queryFullPath, f)) ]

		if list_indicator == False or list_indicator == 'output':
			print 'Loading file list from output folder.'
			# list everything from @outputFullPath and append to @ outputFilenameList if it is a file
			self.outputFilenameList = [ f for f in listdir(self.outputFullPath) if isfile(join(self.outputFullPath, f)) ]


	######################### CLASS METHOD ##########################
	#####                       runBlast                        #####
	#################################################################
	# Load the indicated filename list into class properties
	# run BLAST using according to class parameters
	#################################################################
	def runBlast(self):
		# Loop through each subject file to match with query file
		for subject_filename in self.subjectFilenameList:
			# ================== LOCAL VARIABLES ====================
			# @subject_filename[string]: a temp for each reference file
			# @subject_id[string]: the id indicating the reference file
			# @outfmt_command[string]: hold the string to deal with the output format
			# =======================================================
			# Get id of this reference file from its filename
			subject_id = subject_filename[:subject_filename.find('.')]

			# Loop through each query file and run BLAST
			for query_filename in self.queryFilenameList:
				# ================ LOCAL VARIABLES ==================
				# @query_filename[string]: a temp for each input file
				# @query_id[string]: the id indicating the input file
				# @command[string]: the command use to run BLAT
				# ===================================================
				# Get id of this input file from its filename
				query_id = query_filename[:query_filename.find('.')]
				
				# Use 'blastn' as defult if the program to be use is not specify
				if self.programToUse == '':
					self.programToUse = 'blastn'
				# Check the program tp be run and give a proper running command	
				if self.programToUse in ['blastn', 'blastp']:
					# Check output format
					if self.outputFormatToUse == 'tabular' or self.outputFormatToUse == 'commented_tabular' or self.outputFormatToUse == 'csv':
						# If output format is tabular or csv
						outfmt_command = "'" + str(self.outputFormats[self.outputFormatToUse])
						# Check if user specify header or not
						if len(self.outputHeaderColumn) > 0:
							# Append headers if it is specify by user
							for a_header in self.outputHeaderColumn:
							# ========== LOCAL VARIABLES ============
							# @query_filename[string]: a temp for each input file
							# =======================================
								outfmt_command += " " + a_header
						outfmt_command += "'"
					elif self.outputFormatToUse in self.outputFormats:
						# If output format is other, just get the format number
						outfmt_command = str(self.outputFormats[self.outputFormatToUse])
					else:
						# Use 'pairwise' as default if output format not specify or not exists
						outfmt_command = str(self.outputFormats['pairwise'])

					# Construct the command for running
					command = '%s -subject %s -query %s -out %s -outfmt %s %s' % \
												( self.programPath + self.programToUse,
												  self.subjectFullPath + subject_filename,
												  self.queryFullPath + query_filename,
												  self.outputFullPath + '%s.subject_%s.blast' % (query_id, subject_id),
												  outfmt_command,
												  self.programParameterString )
				# Print message to the terminal
				print 'Running BLAST "%s" on Subject: %s' % (self.programToUse, subject_filename)
				print '                      and Query: %s' % (query_filename)
				system(command)
		# print ending message to terminal
		print 'BLAST completed.'


	######################### CLASS METHOD ##########################
	#####                      parseOutput                      #####
	#################################################################
	# parse the information from the output files into dictionary
	# ========================== INPUT ==============================
	# @firstToParse[int]: index in @outputFilenameList where the parsing should begin
	# @lastToParse[int]: index in @outputFilenameList where the parsing should end
	# ========================= OUTPUT ==============================
	# @result_dict[dictionary]: the dictionary containing BLAST results
	#								key- ID of the blast result
	#								value- list containing all hit results
	#################################################################
	def parseOutput(self, firstToParse = False, lastToParse = False):
		# Check if the type of output file is supported or not
		if self.outputFormatToUse == 'tabular' or self.outputFormatToUse == 'commented_tabular' or self.outputFormatToUse == 'csv':
			# ==================== LOCAL VARIABLES ======================
			# @parsing_filename_list[list]: list of files to parse the information
			# ===========================================================
			# Initiate @result_dict to contain the parsed result
			result_dict = {}
			# Reload output file names
			self.loadFilenameList('output')

			# Get the list of files to parse from @outputFilenameList
			if firstToParse and lastToParse:
				parsing_filename_list = self.outputFilenameList[firstToParse:lastToParse + 1]
			elif firstToParse and not lastToParse:
				parsing_filename_list = self.outputFilenameList[firstToParse:]
			elif not firstToParse and lastToParse:
				parsing_filename_list = self.outputFilenameList[:lastToParse + 1]
			else:
				parsing_filename_list = self.outputFilenameList

			# Loop through each output to be parse and parse the file
			for an_output in parsing_filename_list:
				# ================ LOCAL VARIABLES ==================
				# @an_output[string]: temp for filename of the output file
				# @output_id[string]: BLAST result output ID
				# ===================================================
				# Get BLAST result ID
				output_id = an_output[ : an_output.find('.blast')]
				# Initiate list to keep parsed data
				result_dict[output_id] = []

				# Print progress message
				print 'Parsing output ' + output_id

				with open(join(self.outputFullPath, an_output)) as blast_result_file:
					# ============== LOCAL VARIABLES ================
					# @blast_result_file[file]: file object of @an_output
					# ===============================================
					for a_line in blast_result_file:
						# ============ LOCAL VARIABLES ==============
						# @a_line[string/list]: a temp for each line in @blast_result_file
						# @seperator[string]: contain a character use to seperate each column of information
						# ===========================================
						# Ignore this line if it is the comment line
						if a_line.startswith('#'):
							continue
						# Get the seperator character for this type of output
						seperator = self.outputSeperator[self.outputFormatToUse]
						# Strip newlines and split into list
						a_line = a_line.strip('\n').split(seperator)
						# Append the line into @result_dict
						result_dict[output_id].append(a_line)

			return result_dict
		else:
			# If the parsing is not supported, print message and return
			print 'Parsing not support for '+ self.outputFormatToUse +' output format....not yet.'
			return False
