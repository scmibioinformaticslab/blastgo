# blastGo README
version 0.0.0

---
`blastGo` is a **python class** use to automate and simplified the usage of BLAST programs from `ncbi-blast+` package.

##### FEATURES
- Iteratively run BLAST on your set of subject sequences and query sequences.
- Parse BLAST output file and return data in dictionary for further usage.
##### RESTRICTION
- Currently support only `blastn` and `blastp` within `ncbi-blast+` package.
- Currently support only `pairwise`, `xml`, `tabular`, `csv`, and `tabular with header`.
    > If you need the output in other formats, you have to specify it manually in parameter string setting.
- Currently support only the parsing of `tabular`, `csv`, and `tabular with header`.
- The system is only tested on *Linux Ubuntu*, bugs might emerge upon using on other OS.

---
### USING blastGo
##### IMPORTING
To use `blastGo`, simply copy the `blastGo.py` from this repository to your desire directory, the class can be import into your python code using:
```python
from blastGo import blastGo
```
If you wish for `blastGo.py` to be contain in the children directory of your python script, don't forget to add a blank `__init__.py` to the directory so that python could locate the folder. Then you can import the class to your python script with:
```python
from foldername.blastGo import blastGo
```
##### PREPARING YOUR FILES
**Input sequences**: All of your subject sequences should be in a single folder, with NO OTHER FILES mixed in. The same goes for your query sequences.
##### INITIALIZE blastGo INSTANCE
There are several parameters you need to configure before the instance can be use.

- **Query files path**: This indicate the folder containing all your query sequence file.
- **Subject files path**: This indicate the folder containing all the subject sequence file.
- **Output files path**: This indicate the folder your BLAST output will be written to.
- **Program to use**: This indicate the program to be used. Can be either `blastn` or `blastp`.
- **Output format to use**: This indicate format of the output files.
- **Parameter string**: Additional BLAST parameter you wish to specify. See [BLAST manual](http://www.ncbi.nlm.nih.gov/books/NBK279690/) for this one.

For more information on each parameters, please see [the source code](https://bitbucket.org/scmibioinformaticslab/blastgo/src/42aa1dcec351ab8b0b98050e948dfc889402266a/blastGo.py?fileviewer=file-view-default).


---
### CONTRIBUTION GUIDELINES
If you wish to contribute in the development of `blastGo` or use it as a base of your very own repository, please create a **pull request**, or **fork** this repository.

Additonally, before merging, please make sure that your code are:

- Throughoutly commented in the same manner as the original master branch.
- Tested with no major bugs.

---
### CONTACTS
For additional information, question, issue, or suggestion, please contact or go to:

- *Panawun PALITTAPONGARNPIM* [[panawun.p@gmail.com]( mailto:panawun.p@gmail.com )]